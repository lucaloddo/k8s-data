import './App.css'
import { Route, Routes } from 'react-router-dom'
import LoginPage from './pages/auth/LoginPage'
import Authenticate from './services/auth/Authenticate'
import InventoryPage from './pages/inventory/IntentoryPage'
import DashboardLayout from './layout/DashboardLayout'
import NotFoundPage from './layout/NotFoundPage'
import TestFaas from './pages/faas/TestFaas'
import CreateProduct from './pages/inventory/CreateProduct'

function App() {

  return (
    <div>
      <Routes>
        <Route exact path="/login" element={<LoginPage />} />
        <Route exact path="/" element={<Authenticate />}>
          <Route exact path="/" element={<DashboardLayout />}>
            <Route index path="/" element={<InventoryPage />} />
            <Route index path="/create" element={<CreateProduct />} />
            {/* <Route exact path="/anagrafiche" element={<AnagraficaPage/>}>
              <Route index path="/anagrafiche" element={<AnagraficaTable/>} />
              <Route exact path="/anagrafiche/create" element={<AnagraficaDetail/>} />
              <Route exact path="/anagrafiche/:id" element={<AnagraficaDetail/>} />
            </Route> */}
            <Route exact path="/dropbox" element={<TestFaas />} />
          </Route>
        </Route>
        <Route path="*" exact={true} element={<NotFoundPage />} />
      </Routes>
    </div>
  )
}

export default App